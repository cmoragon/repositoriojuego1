package aplicacion;
import java.util.Scanner;
import java.util.Random;
import java.lang.NumberFormatException;
public class Principal{
	//static es para que se pueda utilizar sin crear un objeto
	//final es lo que mas se acerca a declarar un numero constante en java

	private static final int ESTADO_NOMBRE = 0;
	private static final int ESTADO_NUMERO_MAX = 1;
	private static final int ESTADO_NUMERO_INTRODUCIDO = 2;
	private static final int ESTADO_VOLVER_A_JUGAR = 3;
	private static final int ESTADO_FIN = 4;
	private static int estado = -1;

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		Random rand = new Random();

		String answer = "";
		String nombre = "";
		String respuesta = "";
		int numeroMax = 0;
		int numeroIntroducido = 0;
		int numeroRandom = 0;
		System.out.println("Bienvenido jugador");
		printHelp();

		estado = ESTADO_NOMBRE;
		while(estado == ESTADO_NOMBRE){
			System.out.println("¿Como te llamas?");
			answer = sc.next();
			if(!palabraClave(answer)){
				nombre = answer;
				estado = ESTADO_NUMERO_MAX;
			}
		}

		estado = ESTADO_NUMERO_MAX;
		while(estado == ESTADO_NUMERO_MAX){
			System.out.println("Introduce un numero maximo mayor que 2");
			answer = sc.next();
			numeroMax = Integer.parseInt(answer);
			if (!palabraClave(answer)){
				try{
					if(numeroMax >= 2){
						numeroRandom = rand.nextInt(numeroMax) + 1;
						estado = ESTADO_NUMERO_INTRODUCIDO;
					} else {
						System.out.println("El numero tiene que ser mayor a 2.");
					}
				}catch(NumberFormatException e){
					System.out.println("Prueba otra vez");
				}
			}	
		}

		while(estado == ESTADO_NUMERO_INTRODUCIDO){
			System.out.println("Ahora intenta adivinar el número entre el 1 y el " + numeroMax);
			answer = sc.next();
			numeroIntroducido = Integer.parseInt(answer);
			if(!palabraClave(answer)){
				try{
					if(numeroIntroducido == numeroRandom){
						System.out.println("¡Genial! Has acetado");
						estado = ESTADO_VOLVER_A_JUGAR;
					}else if(numeroIntroducido < numeroRandom){
						System.out.println("El numero que buscas es mayor");
						estado = ESTADO_NUMERO_INTRODUCIDO;
					}else if(numeroIntroducido > numeroRandom){
						System.out.println("El numero que buscas es menor");
						estado = ESTADO_NUMERO_INTRODUCIDO;
					}
				}catch(NumberFormatException p){
					System.out.println("Prueba otra vez");
				}
			}
		}


		while(estado == ESTADO_VOLVER_A_JUGAR){
			System.out.println("¿Quieres volver a jugar?");
			answer = sc.next();
			respuesta = answer;
			if(!palabraClave(answer)){
				//try{
				if(respuesta.equalsIgnoreCase("y")){
					estado = ESTADO_NUMERO_MAX;
				}else if(respuesta.equalsIgnoreCase("n")){
					estado = ESTADO_FIN;
				} else {
					estado = ESTADO_VOLVER_A_JUGAR;
				}
				//}catch(
			}
		}
	}
	private static boolean palabraClave(String answer){
		if(answer.equalsIgnoreCase("help")){
			printHelp();
			return true;
		} else if (answer.equalsIgnoreCase("exit")){
			estado = ESTADO_FIN;
			return true;
		}
		return false;
	}

	private static void printHelp(){
		System.out.println("Puedes poner la palabra 'help' para que te salgan las normas del juego \n Tambien puedes poner 'exit' para salir del juego");
	}
}
