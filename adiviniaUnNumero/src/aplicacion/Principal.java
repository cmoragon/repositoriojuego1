package aplicacion;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Random;
import java.lang.NumberFormatException;
public class Principal{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		//se ouede añadir dentro de () una semilla. semilla = numero. Lo que nos hace es q para la misma semilla siempre mismo numero
		//Random r = new Random(123);
		boolean valor1 = true;
		boolean valor2 = true;
		boolean valor3 = true;
		int numeroIntentos = 0;
		String normas = "Las normas del juego aleatorio son las siguientes: " + "\n" + "Primero di tu nombre para saber quien es el jugador." + "\n" + "Después pon un número mayor a 2." + "\n" + "El ordenador generara un número aleatorio entre 1 y el numero que has dicho." + "\n" + " Por ultimo, ¡intenta adivinarlo!";
		

		System.out.println("¡Hola!" + "\n" + "¿Como quieres que te llame?");
		String nombre = sc.next();
		while(valor1){
			try{
				System.out.println(nombre + ", dime un numero entero mayor o igual a 2");
				String numeroMax = sc.next();
				if(numeroMax.equalsIgnoreCase("help")){
					System.out.println(normas);
					valor1 = true;
				}else if(numeroMax.equalsIgnoreCase("exit")){
					break;
				}else if(Integer.parseInt(numeroMax) < 2){
					System.out.println(nombre + ", prueba de nuevo");
					valor1 = false;
				}else{
					valor1 = true;
					while(valor2){
						//int numeroRandom = (int) (Math.random() * numeroRandom + 1;
						//se pone (int) para ajustar lo que seria un double a int
						//se * numeroMax para ajustar el número máx y un +1 para que no sea desde el 0
						int numeroRandom = r.nextInt(Integer.parseInt(numeroMax)) + 1;
						while(valor3){
							try{
								System.out.println(nombre + ", ahora dime un numero entre 1 y " + numeroMax);
								String numeroPuesto = sc.next();
								if(numeroPuesto.equalsIgnoreCase("help")){
									System.out.println(normas);
									valor3 = true;
								}else if(numeroPuesto.equalsIgnoreCase("exit")){
									break;
								}else if(Integer.parseInt(numeroPuesto) == numeroRandom){
									System.out.println("¡Enhorabuena, " + nombre + " eres genial!");
									if(numeroIntentos == 1 ){
										System.out.println(nombre + " , ¡lo has hecho a la primera!");
									}else{
										System.out.println(nombre + " ,lo has hecho en " + numeroIntentos + " intentos.");
									}
									valor3 = false;
								}else if(Integer.parseInt(numeroPuesto) < numeroRandom && Integer.parseInt(numeroPuesto) >= 1){
									System.out.println(nombre + ", prueba con un numero mayor");
									numeroIntentos += 1;
									System.out.println(nombre + " ,llevas " + numeroIntentos + " intentos.");
									valor3 = true;
								}else if(Integer.parseInt(numeroPuesto) > numeroRandom && Integer.parseInt(numeroPuesto) <= Integer.parseInt(numeroMax)){
									System.out.println(nombre + ", prueba con un numero menor");
									numeroIntentos += 1;
									System.out.println(nombre + " ,llevas " + numeroIntentos + " intentos.");
									valor3 = true;
								}else if(Integer.parseInt(numeroPuesto) < 1){
									System.out.println(nombre + ", el numero tiene que estar entre el 1 y " + numeroMax);
									numeroIntentos += 1;
									System.out.println(nombre + " ,llevas " + numeroIntentos + " intentos.");
									valor3 = true;
								}else if(Integer.parseInt(numeroPuesto) > Integer.parseInt(numeroMax)){
									System.out.println(nombre + ", el numero tiene que estar entre el 1 y " + numeroMax);
									numeroIntentos += 1;
									System.out.println(nombre + " ,llevas " + numeroIntentos + " intentos.");
									valor3 = true;
								}

							}catch(NumberFormatException p){
								System.out.println(nombre + ", prueba otra vez");
							}
						}
						System.out.println(nombre + ", ¿quieres seguir jugando?" + "\n" + "Responde Y(yes) o N(no)");
						String respuesta = sc.next();
						if(respuesta.equalsIgnoreCase("help")){
							System .out.println(normas);
							valor2 = true;
						}else if(respuesta.equalsIgnoreCase("y")){
							System.out.println("¡Genial! Mucha suerte, " + nombre);
							valor2 = true;
						}else if(respuesta.equalsIgnoreCase("n")){
							System.out.println("Espero verte pronto " + nombre);
							valor2 = false;
						}
					}
				}
			}catch(NumberFormatException e){
				System.out.println(nombre + ", prueba de nuevo");
			}
			if(nombre.equalsIgnoreCase("help")){
				System.out.println(normas);
			}
		}sc.close();
	}
}
